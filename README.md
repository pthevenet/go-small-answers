# Go Small Answers

Repository containing some questions and some answers about the Go programming language.

- [Indexing a map using byte slices](./byte-slice-keyed-maps/), explained [here](https://pthevenet.com/posts/programming/go/bytesliceindexedmaps/).

