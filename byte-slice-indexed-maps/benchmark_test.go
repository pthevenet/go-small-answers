package bytesmap

// run with `go test -bench=.`

import (
	"encoding/hex"
	"testing"
)

// byte arrays vs byte slices string

var ok bool
var v interface{}

const (
	mapSize   = int(^uint(0) >> 1)
	arraySize = 10
)

func initialize() (map[[arraySize]byte]interface{}, map[string]interface{}, [arraySize]byte, interface{}) {
	// allocate a large map to avoid reallocations
	mBytes := make(map[[arraySize]byte]interface{}, mapSize)
	mString := make(map[string]interface{}, mapSize)

	key := [arraySize]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	var value interface{}
	value = nil

	return mBytes, mString, key, value

}

func BenchmarkArrayKeyed(b *testing.B) {
	m, _, key, value := initialize()

	b.ResetTimer()
	bArray(m, key, value, b)
}

func BenchmarkOptimizedStringKeyed(b *testing.B) {
	_, m, key, value := initialize()

	b.ResetTimer()
	bOptimizedString(m, key[:], value, b)
}

func BenchmarkStringKeyed(b *testing.B) {
	_, m, key, value := initialize()

	b.ResetTimer()
	bString(m, key[:], value, b)
}

func BenchmarkHexKeyed(b *testing.B) {
	_, m, key, value := initialize()

	b.ResetTimer()
	bHex(m, key[:], value, b)
}

func bArray(m map[[arraySize]byte]interface{}, key [arraySize]byte, value interface{}, b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertArrayKey(m, key, value)
		v, ok = retrieveArrayKey(m, key)
		deleteArrayKey(m, key)
	}
}

func bOptimizedString(m map[string]interface{}, key []byte, value interface{}, b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertOptimizedStringKey(m, key, value)
		v, ok = retrieveOptimizedStringKey(m, key)
		deleteOptimizedStringKey(m, key)
	}
}

func bString(m map[string]interface{}, key []byte, value interface{}, b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertStringKey(m, key, value)
		v, ok = retrieveStringKey(m, key)
		deleteStringKey(m, key)
	}
}

func bHex(m map[string]interface{}, key []byte, value interface{}, b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertHexKey(m, key, value)
		v, ok = retrieveHexKey(m, key)
		deleteHexKey(m, key)
	}
}

// optimized string

func retrieveOptimizedStringKey(m map[string]interface{}, key []byte) (interface{}, bool) {
	v, ok := m[string(key)]
	return v, ok
}

func insertOptimizedStringKey(m map[string]interface{}, key []byte, val interface{}) {
	m[string(key)] = val
}

func deleteOptimizedStringKey(m map[string]interface{}, key []byte) {
	delete(m, string(key))
}

// non-optimized string

var kstr string

func retrieveStringKey(m map[string]interface{}, key []byte) (interface{}, bool) {
	kstr = string(key)
	v, ok := m[kstr]
	return v, ok
}

func insertStringKey(m map[string]interface{}, key []byte, val interface{}) {
	kstr = string(key)
	m[kstr] = val
}

func deleteStringKey(m map[string]interface{}, key []byte) {
	kstr = string(key)
	delete(m, kstr)
}

// byte array

func retrieveArrayKey(m map[[arraySize]byte]interface{}, key [arraySize]byte) (interface{}, bool) {
	v, ok := m[key]
	return v, ok
}

func insertArrayKey(m map[[arraySize]byte]interface{}, key [arraySize]byte, val interface{}) {
	m[key] = val
}

func deleteArrayKey(m map[[arraySize]byte]interface{}, key [arraySize]byte) {
	delete(m, key)
}

// hex convertion

func retrieveHexKey(m map[string]interface{}, key []byte) (interface{}, bool) {
	v, ok := m[hex.EncodeToString(key)]
	return v, ok
}

func insertHexKey(m map[string]interface{}, key []byte, val interface{}) {
	m[hex.EncodeToString(key)] = val
}

func deleteHexKey(m map[string]interface{}, key []byte) {
	delete(m, hex.EncodeToString(key))
}
